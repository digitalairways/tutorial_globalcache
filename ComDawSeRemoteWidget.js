// DOC this function permit to take the json format to be sent by httpPost
toJSONForRemote = function(frenquency, preamble, irCode, repeat){
    var text = {
        "frequency" :  frenquency,
        "preamble" :  preamble,
        "irCode" :  irCode,
        "repeat" :  repeat
    };
    return text;
};

// DOC this function permit to create a table of details of current device
getInfoForRemote = function(make, model, host, firmwareVersion, boardVersion){
    var table = [
        "maker : " + make ,
        "model : " + model,
        "host : " + host,
        "firmware version : " + firmwareVersion,
        "board version : " + boardVersion
    ];
    return table;
};

var ComDawSeRemoteWidget = function(box,optionClass) {
    ComDawSeRemoteWidget.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidget, Thing);

ComDawSeRemoteWidget.prototype.init = function(){
    this.setImage(this.spec.backRemote,this.spec.w,this.spec.h);
    var par = this;
    this.option.imageReady = function(){
        par.spec.wait = true;
    };
};
ComDawSeRemoteWidget.prototype.onPaint = function() {
    var scene = getCurrentScene();
    /*
     var img = graphics_getImage(this.spec.url);
     var img2 = resizeImage(img,900,500);
     graphics_drawImgGraphic(img2,this.getX(), this.getY());
     Graphics_free(img);
     Graphics_free(img2);
    */
    scene.setColor("0xFFFFFFFF");
    scene.drawRectangle(this.getX(), this.getY(),this.spec.w,this.spec.h);
    Thing.prototype.onPaint.call(this);
};



/*
 Info Thing
 */

var ComDawSeRemoteWidgetInfo = function(box,optionClass) {
    ComDawSeRemoteWidgetInfo.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetInfo, Thing);
ComDawSeRemoteWidgetInfo.prototype.init = function() {
    // We assume parameters are well formatted for a tutorial

    this.par = this.spec.screen['infoScreen'];
    this.base = this.spec.screen['base'];

    var info = httpGet("http://"+ this.base.spec.GlobalCacheIP +"/api/v1/version");
    info.parent = this;
    info.onSuccess = function(){
        var bdy = JSON.parse(this.getBody());
        var sent = getInfoForRemote(bdy.make,bdy.model,bdy.host,bdy.firmwareVersion,bdy.boardVersion);
        this.parent.par.spec.text = sent;
    };
};

ComDawSeRemoteWidgetInfo.prototype.onClick = function() {
    if(this.par.spec.hidden){
        this.par.show();
        this.par.spec.hidden = false;
    }else {
        this.par.hide();
        this.par.spec.hidden = true;
    }
};
ComDawSeRemoteWidgetInfo.prototype.onPaint = function() {
    Thing.prototype.onPaint.call(this);
};

/*
 OnOff Thing
 */

var ComDawSeRemoteWidgetOnOff = function(box,optionClass) {
    ComDawSeRemoteWidgetOnOff.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetOnOff, Thing);
ComDawSeRemoteWidgetOnOff.prototype.onClick = function() {
    this.base = this.spec.screen['base'];
    var soc = toJSONForRemote(this.spec.frequency, this.spec.preamble, this.spec.irCode, this.spec.repeat);
    httpPost("http://"+ this.base.spec.GlobalCacheIP +"/api/v1/irports/1/sendir",JSON.stringify(soc));
};
ComDawSeRemoteWidgetOnOff.prototype.onPaint = function() {
    Thing.prototype.onPaint.call(this);
};

/*
 Decrease Thing
 */

var ComDawSeRemoteWidgetDecrease = function(box,optionClass) {
    ComDawSeRemoteWidgetDecrease.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetDecrease, Thing);
ComDawSeRemoteWidgetDecrease.prototype.onClick = function() {
    this.base = this.spec.screen['base'];
    var soc = toJSONForRemote(this.spec.frequency, this.spec.preamble, this.spec.irCode, this.spec.repeat);
    httpPost("http://"+ this.base.spec.GlobalCacheIP +"/api/v1/irports/1/sendir",JSON.stringify(soc));

};
ComDawSeRemoteWidgetDecrease.prototype.onPaint = function() {
    Thing.prototype.onPaint.call(this);
};

/*
 Mute Thing
 */

var ComDawSeRemoteWidgetMute = function(box,optionClass) {
    ComDawSeRemoteWidgetMute.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetMute, Thing);
ComDawSeRemoteWidgetMute.prototype.onClick = function() {
    this.base = this.spec.screen['base'];
    var soc = toJSONForRemote(this.spec.frequency, this.spec.preamble, this.spec.irCode, this.spec.repeat);
    httpPost("http://"+ this.base.spec.GlobalCacheIP +"/api/v1/irports/1/sendir",JSON.stringify(soc));
};
ComDawSeRemoteWidgetMute.prototype.onPaint = function() {
    Thing.prototype.onPaint.call(this);
};

/*
 Increase Thing
 */

var ComDawSeRemoteWidgetIncrease = function(box, optionClass) {
    ComDawSeRemoteWidgetIncrease.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetIncrease, Thing);
ComDawSeRemoteWidgetIncrease.prototype.onClick = function() {
    this.base = this.spec.screen['base'];
    var soc = toJSONForRemote(this.spec.frequency, this.spec.preamble, this.spec.irCode, this.spec.repeat);
    httpPost("http://"+ this.base.spec.GlobalCacheIP +"/api/v1/irports/1/sendir",JSON.stringify(soc));
};
ComDawSeRemoteWidgetIncrease.prototype.onPaint = function() {
    Thing.prototype.onPaint.call(this);
};

/*
 Hidden Thing
 */

var ComDawSeRemoteWidgetHidden = function(box,optionClass) {
    ComDawSeRemoteWidgetHidden.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetHidden, Thing);
ComDawSeRemoteWidgetHidden.prototype.init = function() {
    this.hide();
};
ComDawSeRemoteWidgetHidden.prototype.onPaint = function() {
    var scene = getCurrentScene();
    scene.setFont(this.spec.font);
    scene.setTextColor(this.spec.color);
    var y = 0;
    for(var i = 0 ; i < this.spec.text.length ; i++){
        scene.drawString(this.spec.text[i],this.getX(),this.getY()+y);
        y += this.spec.yOffset;
    }
    Thing.prototype.onPaint.call(this);
};


/*
Waiting thing
 */

var ComDawSeRemoteWidgetWaiting = function(box,optionClass) {
    ComDawSeRemoteWidgetWaiting.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeRemoteWidgetWaiting, Thing);
ComDawSeRemoteWidgetWaiting.prototype.init = function() {
    this.time = 0;
    this.txt = "Waiting image ";
    this.point = ".";
};
ComDawSeRemoteWidgetWaiting.prototype.onPaint = function() {
    var parent = this.spec.screen['base'];
    var scene = getCurrentScene();
    scene.setTextColor(this.spec.color);
    scene.setFont(this.spec.font);
    var x = this.getX() + parent.spec.w/2 - scene.getTextWidth(this.txt)/2;
    var y = this.getY() + parent.spec.h/2 - scene.getTextHeight(this.txt)/2;
    scene.setColor("0x99ABABAB");
    if(!parent.spec.wait){
        scene.drawRectangle(parent.spec.x,parent.spec.y,parent.spec.w, parent.spec.h);
        scene.drawString(this.txt+this.point,x,y);
        this.time++;
        if(this.time == 1) {
            this.point = "..";
        }
        else if(this.time == 2) {
            this.point = "...";
        }
        else{
            this.point = ".";
            this.time = 0;
        }
        this.refresh(1000);
    }
};

